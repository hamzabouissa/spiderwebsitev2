import requests
from bs4 import BeautifulSoup   
import re
import json
import aiohttp
import asyncio
import time
import logging
from app.models import Hotel,HotelPrice
from datetime import date,timedelta
from dataclasses import dataclass
from celery import shared_task
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.core.exceptions import  ObjectDoesNotExist

logger = logging.getLogger(__name__)


@dataclass
class Url:
    
    checkin_date:date
    checkout_date:date
    base_url:str="https://www.booking.com/searchresults.en-us.html?checkin_month={}&checkin_monthday={}&checkin_year={}&checkout_month={}&checkout_monthday={}&checkout_year={}&group_adults=2&group_children=0&no_rooms=1&ss_raw=Mecca&dest_id=-3096949&dest_type=city&place_id_lat=21.422499&place_id_lon=39.826191&rows=25"
    room:int=1
    offset:int=0
    
    @property
    def get_url(self):
        return self.base_url.format(self.checkin_date.month,self.checkin_date.day,self.checkin_date.year,self.checkout_date.month,self.checkout_date.day,self.checkout_date.year)





class Spider:

    def __init__(self,urls):
        
        self.headers = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5"
        }
        self.urls = urls
        self.hotelsData = []
        self.num = 0
        self.g_thread_limit = asyncio.Semaphore(10)
        self.session = None

    async def crawler(self,url):
        
        new_url = url.get_url + f"&offset={url.offset}"

        async with self.session.get(new_url,headers=self.headers) as response:
            logger.info(f"fetching day {url.checkin_date} at page : {url.offset}")
            try:
                page =  await response.text()
            except aiohttp.ClientPayloadError:
                print("error occured")
            else:
                self.scraper(page,url)
        
        if  len(asyncio.all_tasks())==2:
            await self.session.close()
            logger.error(f"number of pages {self.num} ")
            self.save()
            print("saved ...")
            


    
    async def intermediare(self,url): # This for limiting requests
        async with self.g_thread_limit:
            await self.crawler(url)


    async def main(self):
        if not self.session:
            async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False)) as self.session:
                data = await asyncio.gather(*[
                    self.intermediare(url)
                    for url in self.urls
                ])
            

    
    def scraper(self,page,url):

        find = False
        html = BeautifulSoup(page,"lxml")
        next_page = html.find("a",title='Next page')

        # Find the script that holds the data
        for sc in html.find_all("script"):
            script = str(sc)
            try:
                data = script[script.index("hotel_info_prices"):script.index("b_wholesaler_hotels_cheapest_prices")].strip().replace("\n","")
            except:
                pass
            else:
                find=True
                break
        # Convert data to dictionary or Json

        if find:

            data = data[data.index("{"):-3] + "}"
            self.hotels_js_data = json.loads(data)
            for key,_ in self.hotels_js_data.items():
                if self.hotels_js_data[key]:
                    #if not self.redis_conn.sismember('booking_ids',key):
                    try:
                        ht = Hotel.objects.get(booking_id=key)
                    except ObjectDoesNotExist:
                        ht = self.hotel_info(html,key)
                   
                    if ht:
                        self.hotelsData.append(
                            HotelPrice(
                                hotel = ht,
                                start_date = url.checkin_date,
                                final_date = url.checkout_date,
                                price = round(float(self.hotels_js_data[key]))*url.room,
                                room = url.room
                            )
                        )
                    self.num+=1
                    

        if next_page and url.offset>175:
            url.offset+=25
            asyncio.create_task(self.crawler(url))
        
        

    def hotel_info(self,html,id):
        
        # Search by Hotel Key for its name

        hotel_div = list(html.find("div",id='hotel_' + str(id)).next_siblings)[1]
        title = hotel_div.find("span",class_="sr-hotel__name")
        
        
        stars = hotel_div.find(class_='bk-icon-wrapper bk-icon-stars star_track')
        if stars:
            stars = int(stars.attrs['title'].split("-")[0])
        else:
            stars = 0
        distance = hotel_div.find("span",attrs={"data-tooltip-position":True}).string
        if distance:
            distance,s = distance.strip().split(" ")[0:2]
            distance = float(distance)
            if s=='m':
                distance /=1000
            
            #self.redis_conn.sadd('booking_ids',id)
            return Hotel.objects.create(
                booking_id = id,
                name=title.string.strip(), 
                stars = stars,
                distance =  distance,
            )
        return None
    
    def save(self):
        HotelPrice.objects.bulk_create(self.hotelsData)
    
    



    @classmethod
    async def init(cls,start_date,final_date,period):

        urls = []
        period = period
        checkin_date = start_date
        checkout_date = checkin_date + timedelta(days=period)

        while(checkin_date<final_date):
            for i in range(0,200,25):
                url = Url(checkin_date,checkout_date,offset=i)
                urls.append(url)
            checkin_date+=timedelta(days=1)
            checkout_date = checkin_date + timedelta(days=period)

        
        sc = cls(urls)
        await sc.main()


@shared_task
def notify(**kwargs):
    message = Mail(from_email='bouissihamza6@gmail.com',
            to_emails=kwargs.get('email'),
            subject='Hotels Data',
            html_content=f'We received You request,here is the <a href=\'http://142.93.98.17:8001/HotelsName?start_date={kwargs.get("start_date")}&final_date={kwargs.get("final_date")}&period={kwargs.get("period")}\'>Link</a>')
    try:
        sg = SendGridAPIClient('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')
        sg.send(message)
    except Exception as e:
        print(e)


@shared_task
def run(**kwargs):

    s = date.fromisoformat(kwargs.pop('start_date'))
    f = date.fromisoformat(kwargs.pop('final_date'))

    # try:
    #     loop = asyncio.get_running_loop()
    # except RuntimeError:
    #     loop = asyncio.run(Spider.init(s,f,kwargs.get('period')))
    # else:
    #     asyncio.create_task(Spider.init(s,f,kwargs.get('period')))
    loop = asyncio.run(Spider.init(s,f,**kwargs))