from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
import logging
from .tasks import run
from django.http import QueryDict
from datetime import date,timedelta
from celery import group
from rest_framework.renderers import TemplateHTMLRenderer
from app.models import Hotel,HotelPrice
from app.serializers import HotelSerializer,HotelsNameSeriliazer,HotelRequestSerializer
from django.db.models import F  



# Create your views here.

logger = logging.getLogger()

class Crawler(APIView):


    #serializer_class = HotelRequestSerializer


    def post(self,request,*args,**kwargs):
        
        sr = HotelRequestSerializer(request.data)
        run(**sr.data)
        data = Hotel.objects.all()   
        context = {}
        context['final_date'] = sr.data['final_date']
        context['start_date'] = sr.data['start_date']
        context['period'] =sr.data['period']
        ht_ser = HotelsNameSeriliazer(data,many=True,context=context)
        
        return Response(ht_ser.data)






class HotelData(APIView):


    def get(self, request):
        data = request.GET
        
        booking_id = int(data.get('booking_id',None))
        hotel = Hotel.objects.get(booking_id = booking_id)
        period = int(data.get('period',None))
        start_date = date.fromisoformat(data.get('start_date',None))
        final_date = date.fromisoformat(data.get('final_date',None))
        
        ht = HotelPrice.objects.filter(start_date__lte=final_date,start_date__gte=start_date,hotel=hotel).annotate(r=F('final_date')-F('start_date')).filter(r__day=period).order_by('start_date')
        ht_ser = HotelSerializer(ht,many=True)

        return Response(ht_ser.data)


class HotelsName(APIView):


    def get(self,request):
        data = Hotel.objects.all()   
        context = {}
        context['final_date'] = request.GET['final_date']
        context['start_date'] = request.GET['start_date']
        context['period'] = request.GET['period']
        ht_ser = HotelsNameSeriliazer(data,many=True,context=context)
        return Response(ht_ser.data)
