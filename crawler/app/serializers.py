from .models import Hotel,HotelPrice
from rest_framework import serializers


class HotelSerializer(serializers.ModelSerializer):

    class Meta:
        model = HotelPrice
        fields = ['fetching_date','start_date','final_date','price','room']



class HotelsNameSeriliazer(serializers.ModelSerializer):

    link = serializers.CharField(max_length=12,default='Test')

    class Meta:
        model = Hotel
        fields = ['name','link']
    
    def to_representation(self, instance):
        """Convert `username` to lowercase."""
        ret = super().to_representation(instance)
        final_date = self.context['final_date']#.strftime("%Y-%m-%d")
        start_date = self.context['start_date']
        period = self.context['period']
        link = f'http://142.93.98.17:8001/HotelData?booking_id={instance.booking_id}&start_date={start_date}&final_date={final_date}&period={period}'
        ret['link'] = link
        return ret
    
    
   
class HotelRequestSerializer(serializers.ModelSerializer):


    period = serializers.IntegerField()

    class Meta:

        model = HotelPrice
        fields = ['start_date','final_date','period','room']

