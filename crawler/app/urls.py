from django.urls import path,include

from app import views


urlpatterns = [
    path('crawler',views.Crawler.as_view(),name='crawler'),
    path('HotelData',views.HotelData.as_view()),
    path('HotelsName',views.HotelsName.as_view()),


]
