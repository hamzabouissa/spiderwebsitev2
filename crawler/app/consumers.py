from channels.generic.websocket import WebsocketConsumer,JsonWebsocketConsumer
import json
from app.models import Hotel
from app.serializers import HotelSerializer
import logging
from datetime import date,timedelta
from celery import group
from app.tasks import search
from asyncio import sleep


logger = logging.getLogger()
class CrawlerConsumer(JsonWebsocketConsumer):
    groups = ["broadcast"]

    def connect(self):
        # Called on connection.
        # To accept the connection call:
        self.accept()
        

    def crawler(self,data):

        period = int(data.get('period'))
        start_date = date.fromisoformat(data.get('start',None))
        end_start_date = date.fromisoformat(data.get('end',None))

        duration = end_start_date-start_date
        s = start_date
        urls = []
        for _ in range(0,duration.days):
            final_date = start_date+timedelta(period)
            urls.append([f"https://www.booking.com/searchresults.en-us.html?label=gen173nr-1FCAEoggI46AdIM1gEaOIBiAEBmAExuAEZyAEP2AEB6AEB-AECiAIBqAIDuALaouPrBcACAQ&sid=7829786ba33e589fac96ba65a819ae22&sb=1&src=index&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Findex.html%3Flabel%3Dgen173nr-1FCAEoggI46AdIM1gEaOIBiAEBmAExuAEZyAEP2AEB6AEB-AECiAIBqAIDuALaouPrBcACAQ%3Bsid%3D7829786ba33e589fac96ba65a819ae22%3Bsb_price_type%3Dtotal%26%3B&ss=Mecca&is_ski_area=0&ssne=Mecca&ssne_untouched=Mecca&dest_id=-3096949&dest_type=city&checkin_month={start_date.month}&checkin_monthday={start_date.day}&checkin_year={start_date.year}&checkout_month={final_date.month}&checkout_monthday={final_date.day}&checkout_year={final_date.year}&group_adults=1&group_children=0&no_rooms=1&b_h4u_keep_filters=&from_sf=1",start_date.strftime("%Y-%m-%d"),final_date.strftime("%Y-%m-%d")])
            start_date+=timedelta(1)
        
        s = group(search.s(url) for url in urls)
        rs = s.apply_async()
        rs.join()
        self.replay(start_date,s,duration)
    
    def receive_json(self, text_data=None, bytes_data=None):
        self.crawler(text_data)

    def replay(self,e,s,duration):
        #data = Hotel.objects.filter(day__lte=e,day__gte=s).annotate(r=F('final_date')-F('day')).filter(r__day=duration).distinct('name')
        data = Hotel.objects.all().distinct('name')
        ht_ser = HotelSerializer(data,many=True)
        self.send_json({'data':ht_ser.data})

    def disconnect(self, close_code):
        # Called when the socket closes
        pass
