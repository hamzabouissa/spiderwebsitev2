from django.db import models

from django.contrib.auth.models import AbstractUser

# Create your models here.

class MyUser(AbstractUser):
    pass


class Hotel(models.Model):

    booking_id = models.IntegerField(default=0,unique=True)
    name = models.CharField(max_length=50,unique=True)
    stars = models.IntegerField(null=True,default=0)
    distance = models.FloatField(null=True)
    
    
    

    #class Meta:
    #    unique_together = ['name', 'day','final_date']
    
    


class HotelPrice(models.Model):
    
    hotel = models.ForeignKey("app.Hotel", on_delete=models.CASCADE)
    start_date = models.DateField(null=False)
    price = models.FloatField()
    final_date = models.DateField(null=False)
    room = models.IntegerField(default=1)
    fetching_date = models.DateTimeField(auto_now=True)

    @property
    def period(self):
        return (self.final_date - self.start_date).days
    
    