#!/bin/bash
source venv/bin/activate
watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- celery -A crawler worker -P gevent -c=10 -l info
 