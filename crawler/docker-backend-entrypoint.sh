#!/bin/bash
if [ ! -e venv ]
then
  virtualenv venv --python=python3.7
fi
source ./venv/bin/activate

pip install -r requirements.txt

python3  manage.py makemigrations
python3 manage.py migrate 
python3 manage.py runserver 0.0.0.0:8001


